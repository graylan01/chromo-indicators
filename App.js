import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, Alert, Animated, Easing } from 'react-native';
import ColorPicker from 'react-native-color-picker';
import SQLite from 'react-native-sqlite-storage';

// Initialize SQLite database
const db = SQLite.openDatabase(
  { name: 'chromographic_indicators.db', location: 'default' },
  () => console.log('Database opened successfully.'),
  (error) => console.error('Failed to open database:', error)
);

const App = () => {
  const [colors, setColors] = useState(['#FF0000', '#00FF00', '#0000FF']);
  const [animation] = useState(new Animated.Value(0));

  // Function to animate welcome screen
  const animateWelcome = () => {
    Animated.timing(animation, {
      toValue: 1,
      duration: 1000,
      easing: Easing.ease,
      useNativeDriver: true,
    }).start();
  };

  // Function to save selected colors to SQLite database
  const handleSaveColors = () => {
    try {
      if (colors.length !== 3) {
        throw new Error('Please select exactly three colors.');
      }

      // Open transaction to save colors
      db.transaction((tx) => {
        tx.executeSql(
          'INSERT INTO indicators (color1, color2, color3) VALUES (?, ?, ?)',
          colors,
          () => console.log('Colors saved to database.'),
          (tx, error) => {
            console.error('Error saving colors:', error);
            Alert.alert('Database Error', 'Failed to save colors.');
          }
        );
      });

      // Navigation or other actions can be performed here
      console.log('Colors saved:', colors);
    } catch (error) {
      Alert.alert('Error', error.message);
    }
  };

  // Function to lock colors and proceed to next screen
  const lockColors = () => {
    // Perform lock action here (not fully implemented in this example)
    console.log('Colors locked:', colors);
  };

  // Function to unlock colors for further customization
  const unlockColors = () => {
    // Perform unlock action here (not fully implemented in this example)
    console.log('Colors unlocked for editing.');
  };

  return (
    <View style={styles.container}>
      {/* Welcome Screen with Animated Transition */}
      <Animated.View style={[styles.welcomeContainer, { opacity: animation }]}>
        <Text style={styles.title}>Chromographic Indicators App</Text>
        <Button
          title="Get Started"
          onPress={() => {
            animateWelcome();
            console.log('Navigate to ColorPicker');
          }}
        />
      </Animated.View>

      {/* How to Use Screen */}
      <View style={styles.howToContainer}>
        <Text style={styles.subtitle}>How to Use</Text>
        <Text style={styles.instructions}>
          Welcome to the Chromographic Indicators App. This app helps enhance navigation and location identification
          using color-based indicators.
        </Text>
        <Text style={styles.instructions}>
          To begin, select three distinct colors that will represent your unique chromographic code.
        </Text>
        <Text style={styles.instructions}>
          These colors will be used to mark locations such as doors, mailboxes, or other designated areas for easy
          identification.
        </Text>
        <Text style={styles.instructions}>
          After selecting your colors, you can lock them to prevent accidental changes or proceed to further customization.
        </Text>
        <Text style={styles.instructions}>
          Ensure the app remains active and the screen stays on for the indicators to work effectively.
        </Text>
        <Button title="Select Colors" onPress={() => console.log('Navigate to ColorPicker')} />
        <Button title="Lock Colors" onPress={lockColors} />
        <Button title="Unlock Colors" onPress={unlockColors} />
      </View>

      {/* Color Picker Screen */}
      <View style={styles.colorPickerContainer}>
        <Text style={styles.subtitle}>Choose Indicator Colors</Text>
        <ColorPicker
          style={styles.colorPicker}
          onColorChange={(color) =>
            setColors((prevColors) => [...prevColors.slice(1), color])
          }
          defaultColor={colors[2]}
        />
        <Button title="Save Colors" onPress={handleSaveColors} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#FFFFFF',
  },
  welcomeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  howToContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  colorPickerContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  subtitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  instructions: {
    textAlign: 'center',
    marginBottom: 10,
  },
  colorPicker: {
    width: 200,
    height: 200,
    marginBottom: 20,
  },
});

export default App;